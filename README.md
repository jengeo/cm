### A Simple Cluster

#### Assignment #1：

Cluster manager(CM) is dealing with Docker containers. You can run following command with CM:

```
python .\CM.py create 8          # create 8 ubuntu containers in the cluster
python .\CM.py list a            # list all containers in the cluster
python .\CM.py list              # list all running containers in the cluster
python .\CM.py start             # start all containers in the cluster
python .\CM.py scale up 2        # add two more containers to the cluster
python .\CM.py scale down 2      # delete two containers from the cluster
python .\CM.py exec echo hello 3 # execute "echo hello" command in 3 containers
python .\CM.py exec echo hello   # execute "echo hello" command in all containers
python .\CM.py stop              # stop all containers in the cluster
python .\CM.py help              # show help information
```

Note: Above commands run on Windows，you can run these commands on other OS accordingly. The cluster use `ubuntu` as the default docker image . Assuming `Docker` ,`Docker SDK for Python`has been installed correctly .More requirements please refer to `requirements.txt`.

