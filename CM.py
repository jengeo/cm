#!/usr/local/bin/python3

import sys
import docker
import time
import json
from os.path import exists
client = docker.from_env()

#show command help message
def show_help_message():
    print("Usage:  ./CM.py COMMAND [N]")
    print()
    print("A cluster manager for docker container management")
    print()
    print("creat [N]          Create N containers in the cluster on docker host, if N is not given N=1")
    print("start              Start all the containers in the cluster")
    print("exec command [N]   Execute command in N containers in the cluster, if N is not given, it will execute in all containers")
    print("scale up/down [N]  Add(up) or remove(down) N containers to(from) the cluster")
    print("list [a]           List running containers in the cluster, if argument a is given list all containers in the cluster")
    print("stop               Stop all containers in the cluster")
    print("prune              Stop all containers and delete them from docker host")
    print("help               Show command help information")

#prune all containers in the cluster
def prune_all_containers():
    json_dic = load_file_to_dic()
    if None != json_dic:
        containers = json_dic.get("containers")
        if None != containers and len(containers) > 0:
            try:
                for c in containers:
                    container = client.containers.get(c["name"])
                    container.stop(timeout=0)
                client.containers.prune()
                print("prune containers success")
            except docker.errors.APIError as e:
                print("prune containers error:{}".format(e))
    print("There is not containers in the cluster")
    json_dic = {}
    save_dic_to_file(json_dic)

# check container status and save
def check_container_status_and_refresh():
    json_dic = load_file_to_dic()
    containers = json_dic.get("containers",[])
    temp_containers = []
    if None != containers and len(containers) > 0:
        for c in containers:
            try:
                name = c["name"]
                client.containers.get(name)
                temp_containers.append(c)
            except docker.errors.NotFound:
                print("container {} has been removed.".format(name))
        json_dic["containers"] = temp_containers
        save_dic_to_file(json_dic)               

#create containers in the cluster and store the clusters information into a file
def create_containers_and_save(cnt, append=False):
    json_dic = {}
    create_time = time.time()
    update_time = create_time
    containers = []
    if append:
        json_dic=load_file_to_dic()
        create_time = json_dic["create_time"]
        update_time = time.time()
        containers = json_dic["containers"]
    json_dic["create_time"] = create_time
    json_dic["update_time"] = update_time
    
    for i in range(0, cnt):
        try:
            image_name = "ubuntu"
            c = client.containers.create(image_name, "sh",tty=True)
            c_dic = { "id":c.id, "image":c.image.tags, "status":c.status, "labels":c.labels, "name":c.name, "short_id":c.short_id}
            containers.append(c_dic)
            print("create container {} success".format(c.name))
        except docker.errors.ImageNotFound:
            print("create container error: image {} not found. ".format(image_name))
        except docker.errors.APIError as e:
            print("create container error: {}".format(e))

    json_dic["containers"] = containers
    save_dic_to_file(json_dic)

def start_or_stop_containers_and_refresh(json_dic, last_arg):
    containers = json_dic.get("containers",[])
    for c in containers:
        name = c["name"]
        try:
            container = client.containers.get(name)
            if "start" == last_arg:
                container.start()
            if "stop" == last_arg:
                container.stop(timeout=0)
            container = client.containers.get(name)  
            c["status"] = container.status
            print("container {} {} success".format(name, last_arg))
        except docker.errors.NotFound:
            print("get {} error: not found this container".format(name))
        except docker.errors.APIError as e:
            print("{} container {} error: {}".format(last_arg ,name ,e))
    save_dic_to_file(json_dic)

# handle and return the running and stopped containers
def filter_running_containers(containers):
    for c in containers:
        container = client.containers.get(c["name"])
        c["status"] = container.status
    stopped_containers = list(filter(lambda cc: cc["status"]=="stopped" or cc["status"]=="exited", containers))
    started_containers = list(filter(lambda cc: cc["status"]!= "stopped" and cc["status"]!= "exited", containers))
    return (stopped_containers, started_containers)

# delete containers and save to cluster.json
def delete_containers_and_save(json_dic, count):
    containers = json_dic.get("containers",[])
    stopped_containers,started_containers = filter_running_containers(containers)
    temp_containers = []
    i = 0
    for c in stopped_containers:
        if (i < count):
            container = client.containers.get(c["name"])
            try:
                container.remove()
                i=i+1
                print("remove container {} success".format(c["name"]))
            except docker.errors.APIError as e:
                print("remove container {} error: {}".format(container.name, e))
        else:
            temp_containers.append(c)
    for c in started_containers:
        if (i < count):
            container = client.containers.get(c["name"])
            try:
                container.stop(timeout=0)
                container.remove()
                i=i+1
                print("remove container {} success".format(c["name"]))
            except docker.errors.APIError as e:
                print("remove container {} error: {}".format(container.name, e))
        else:
            temp_containers.append(c)
    json_dic["containers"] = temp_containers
    save_dic_to_file(json_dic)

# list containers and save containers' status to cluster.json
def list_containers_and_save(json_dic, all):
    containers = json_dic["containers"]
    template_str = "{0:<15}{1:<20}{2:<20}{3:<20}"
    print(template_str.format("CONTAINER ID","IMAGE","STATUS","NAMES"))
    for c in containers:
        container = client.containers.get(c["name"])  
        if "running" == container.status and not all:
            print(template_str.format(container.short_id,container.image.tags[0],container.status,container.name))
        elif all:            
            print(template_str.format(container.short_id,container.image.tags[0],container.status,container.name))
        c["status"] = container.status
    save_dic_to_file(json_dic)

#save cluster info to cluster.json
def save_dic_to_file(json_dic):
    with open("./cluster.json",'w+') as fp:
        json.dump(json_dic, fp)
    
#load cluster info to cluster.json
def load_file_to_dic():
    json_dic = {}
    if exists("./cluster.json"):
        try:
            with open("./cluster.json",'r') as fp:
                json_dic = json.load(fp)
        except:
            print("There doesn't exist any cluster on this host.") 
    return json_dic
        
#handle the command line arguments
def handle_argv():
    argument_cnt = len(sys.argv)
    action = ""
    count = 1
    command = ""
    last_arg = None
    if argument_cnt > 1:
        action = sys.argv[1]
    if argument_cnt > 2:
        last_arg = sys.argv[-1]
        count_str = sys.argv[-1]
        if count_str.isdigit():
            count = int(count_str)
        command = "".join((s+" ") for s in sys.argv[2:argument_cnt])[0:-1]
    return (action, command, count, last_arg)

#execute command in [count] containers
def exec_command(json_dic, command, count):
    print(command, count)
    containers = json_dic.get("containers",[])
    stopped_containers,started_containers = filter_running_containers(containers)
    i = 0
    for c in started_containers:
        if i < count:
            try:
                container = client.containers.get(c["name"])
                exit_code, output = container.exec_run(command)
                print("exec command in container {} and result is :\n{}".format(c["name"], output.decode("utf-8")))
                i=i+1
            except Exception as e:
                print("exec command error:{}".format(e))
        else:
            return
    for c in stopped_containers:
        if i < count:
            try:
                container = client.containers.get(c["name"])
                container.start()
                exit_code, output = container.exec_run(command)
                print("start and exec command in container {} and result is :\n{}".format(c["name"], output.decode("utf-8")))
                i=i+1
            except Exception as e:
                print("exec command error:{}".format(e))
        else:
            return
        


#program entrance
def program_main():
    check_container_status_and_refresh()
    parameters = handle_argv()
    #print(parameters)
    action,command,count,last_arg = parameters
    json_dic = load_file_to_dic()
    containers = json_dic.get("containers", [])
    
    if "create" == action:
        if last_arg.isdigit() or "create"==last_arg:
            create_containers_and_save(count)
            return
        else:
            show_help_message()
            return
    elif "start" == action or "stop" == action:
        if None != containers and len(containers) > 0:
            start_or_stop_containers_and_refresh(json_dic, action)
            return
        if None != last_arg:
            show_help_message()
            return
        print("There is not containers in the cluster")
    elif "scale" == action:
        command = command.replace(str(count), "").replace(" ","")
        if "up" == command and count > 0:
            create_containers_and_save(count, True)
        elif "down" == command and count > 0:
            delete_containers_and_save(json_dic, count)
        else:
            show_help_message()
    elif "list" == action:
        if None != containers and len(containers) > 0:
            command = command.replace(str(count), "").replace(" ","")
            if command == "a":
                list_containers_and_save(json_dic, True)
            else:
                list_containers_and_save(json_dic, False)
        else:
            print("There is no containers in the cluster")
    elif "exec" == action:
        if None != containers and len(containers) > 0:
            command = command.replace(str(count), "")
            if None != last_arg and last_arg.isdigit():
                if (len(containers) >= count):
                    exec_command(json_dic, command, count)
                else:
                   print("There is not enough containers in the cluster")    
            else:
                exec_command(json_dic, command, len(containers))
        else:
           print("There is no containers in the cluster")   

    elif "prune" == action:
        prune_all_containers()
    elif "help" == action:
        show_help_message()
    else:
        show_help_message()

program_main()